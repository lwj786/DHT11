#!/usr/bin/env python3

from sys import argv
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

EXPLAIN = \
''' -h. --help
--csv=FILE save data as csv file.
\tdefault isn't be specified, save file as DHT.csv in current directory.
\tif only specify the directory(means the FILE ends with '/'), the file name will be DHT.csv.
--kind=line|scatter|hist|kde default line
--sub subplot, work for line/scatter
'''

''' func: line()
    and scatter
    '''
def line(dht, kind, is_sub):
    headers = list(dht)
    style = ['b.', 'r^'] if kind == 'scatter' else ['b-', 'r-']

    if is_sub:
        fig, ax = plt.subplots(2, 1)
    else:
        fig, ax = plt.subplots()
        ax = [ax, ax.twinx()]

    for i, col in enumerate(headers):
        dht[col].plot(ax = ax[i] , style = style[i], label = col)

    for a in ax:
        a.xaxis.grid()

    ax[0].yaxis.set_minor_locator(ticker.MultipleLocator(1))
    ax[0].yaxis.grid(which = "both", linestyle = '-')
    ax[0].set_ylabel('/'.join([headers[0], '%']), color = 'b')

    ax[1].yaxis.grid(linestyle = ':')
    ax[1].set_ylabel('/'.join([headers[1], '°C']), color = 'r')

    if not is_sub:
        h1, l1 = ax[0].get_legend_handles_labels()
        h2, l2 = ax[1].get_legend_handles_labels()
        plt.legend(handles = h1 + h2, labels = l1 + l2, loc = 0)

''' func: dist_plot()
    plot distribution of data
    '''
def dist_plot(dht, kind):
    headers = list(dht)
    color = ['b', 'r']

    fig, ax = plt.subplots(2, 1)

    for i, col in enumerate(headers):
        dht[col].plot(ax = ax[i] , kind = kind, color = color[i])

    ax[0].set_xlabel('/'.join([headers[0], '%']), color = 'b')
    ax[1].set_xlabel('/'.join([headers[1], '°C']), color = 'r')

''' func: plot()
    '''
def plot(dht, kind, is_sub):
    if kind == "hist" or kind == "kde":
        dist_plot(dht, kind)
    else:
        line(dht, kind, is_sub)


''' begin
    '''
csv_file, kind, is_sub = '', 'line', False
for arg in argv:
    if arg[:len("--csv=")] == "--csv=":
        csv_file = arg[len("--csv="):]
    if arg[:len("--sub")] == "--sub":
        is_sub = True
    if arg[:len("--kind=")] == "--kind=":
        kind = arg[len("--kind="):]
    elif arg == "--help" or arg == "-h":
        print(EXPLAIN)
        exit()

if not csv_file or csv_file[-1] == '/':
    csv_file += "DHT.csv"

dht = pd.read_csv(csv_file) 

dht['Date'] = pd.to_datetime(dht['Date'])
dht.set_index('Date', inplace = True)

plot(dht, kind, is_sub)

plt.tight_layout()
plt.show()