#!/usr/bin/env python3

# use DHT11 sensor and raspberry pi3 to get humidity and temperature of environment
# refer to datasheet and some code from internet

import RPi.GPIO as GPIO
from time import sleep as delay
import time
import os

EXPLAIN = \
''' -h, --help
--io=<num> set the GPIO code
--max-try=<num> set the max times of try
--csv, --csv=FILE save data as csv file.
\tif FILE isn't be specified, save file as DHT.csv in current directory.
\tif only specify the directory(means the FILE ends with '/'), the file name will be DHT.csv.

Do not measure too much frequently

DHT11       range        precision    accuracy
humidity    20%-95%      ±1%          ±5%(25°C)
temperature 0°C-50°C     ±1°C         ±2°C(25°C)
'''

''' func: cook()
    tansform raw data to 'cooked' data ;p '''
def cook(raw_data):
    data = [0, 0, 0, 0, 0]

    for i in range(5):
        for j, bit in enumerate(raw_data[i * 8:(i + 1) * 8]):
            data[i] += 2 ** (7 - j) * bit

    return data

''' func: read()
    '''
def read(channel):
    # the 40 bit raw data: the first 8 bit of humidity's integer part, then 8 bit of decimal part
    # and following data of temperature is the same form, the last 8 bit is checksum data
    # in fact, the deciaml is always 0
    raw_data = []

    # send triger signal: at least 18 ms
    GPIO.setup(channel, GPIO.OUT)
    GPIO.output(channel, GPIO.LOW) 
    delay(20e-3)
    GPIO.setup(channel, GPIO.IN)

    delay(20e-6)

    # respond: 80 us low level + 80 us high level
    while not GPIO.input(channel):
        pass
    while GPIO.input(channel):
        pass

    # receive raw_data: 50 us low level + 26~28 us high level --> 0, 50 us low level + 70 us high level --> 1
    # after finish sending, 50 us low level
    while len(raw_data) < 40:
        while not GPIO.input(channel):
            pass

        c = 0
        while GPIO.input(channel):
            c += 1
            # timeout check
            if c > 100:
                break
        raw_data.append(0 if c < 8 else 1)

    return raw_data

''' func: measure()
    '''
def measure(channel, count):
    c = 0
    while c < count:
        # the first data is measured in last time, so abandon it
        if not c:
            read(channel)

        date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        delay(2)

        data = cook(read(channel))
        if sum(data[0:3]) == data[4]:
            break
        else:
            print("error in verification")
            data = []
            c += 1

    return date, data

''' func: save_as_csv()
    '''
def save_as_csv(date, data, csv_file):
    csv_file = os.path.expanduser(csv_file)

    if not os.path.exists(csv_file):
        path, file_name = os.path.split(csv_file)

        if path and not os.path.exists(path):
            os.makedirs(path)

    date = date.join(['\"'] * 2)
    data = [str(e) for e in data]
    humi, temp = '.'.join(data[0:2]), '.'.join(data[2:4])

    with open(csv_file, "a+") as csv_file:
        csv_file.seek(0)
        if not csv_file.read():
            csv_file.write(",".join(["Date", "Humidity", "Temperature"]))
            csv_file.write('\n')

        csv_file.write(",".join([date, humi, temp]))
        csv_file.write('\n')

''' func: action()
    '''
def action(channel, _name, params):
    if __name__ != "__main__":
        import stdio_manager as siom

        global print

        index = siom.register(_name)
        print = lambda content: siom.push_output(content, index)

    count, csv_file = 3, ''
    for parameter in params:
        if parameter[:len("--max-try=")] == "--max-try=":
            try:
                count = int(parameter[len("--max-try="):])
            except:
                continue
        elif parameter[:len("--csv")] == "--csv":
            try:
                csv_file = parameter[len("--csv="):]
            except:
                pass

            if not csv_file or csv_file[-1] == '/': 
                csv_file += "DHT.csv"

    channel = channel[0] if type(channel) == list else channel
    date, data = measure(channel, count)

    if len(data):
        print(date)
        print("humidity: %d.%d%%\ntemperature: %d.%d°C" % (data[0], data[1], data[2], data[3]))

        if csv_file: 
            save_as_csv(date, data, csv_file)

    if __name__ != "__main__":
        siom.unregister(index)

''' func: main()
    '''
def main(argv):
    GENERAL_GPIO_BCM_CODE = \
        [4, 17, 18, 27, 22, 23, 24, 25, 5, 6, 12, 13, 19, 16, 26, 20, 21]
    channel = 0

    for arg in argv:
        if arg[:len("--io=")] == "--io=":
            try:
                channel = int(arg[len("--io="):])
            except:
                continue
        elif arg == "--help" or arg == "-h":
            print(EXPLAIN)
            exit()

    if not (channel and channel in GENERAL_GPIO_BCM_CODE):
        exit()

    GPIO.setmode(GPIO.BCM)

    action(channel, "DHT11", argv)

    GPIO.cleanup

if __name__ == "__main__":
    from sys import argv
    main(argv)